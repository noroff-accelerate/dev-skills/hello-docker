# HelloDocker

<img src="https://www.noroff.no/images/docs/vp2018/Noroff-logo_STDM_vertikal_RGB.jpg" alt="banner" width="450"/>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![web](https://img.shields.io/static/v1?logo=icloud&message=Online&label=web&color=success)](https://accelerate-demo-deployment.azurewebsites.net)

A simple dockerized .Net application

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Deployment](#deployment)
- [Configuration](#configuration)
- [Migrations](#database-migrations)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

```
dotnet restore
```

## Usage

The application can be started in development mode using the following command:

```
dotnet run
```

## Deployment

>To replicate this projects CI/CD feature you must copy the [`.gitlab-ci.yml`](./.gitlab-ci.yml) into your project folder which must also contain a working Dockerfile that produces a valid docker image.

The [`.gitlab-ci.yml`](./.gitlab-ci.yml) file will automatically build the associated docker image and upload it to the local gitlab docker registry. The automated deployment job within the script requires additional input which will be configured later.

Deploying the image can be done on any service by pulling the image from the docker registry (using an appropriately privileged Deployment Token &mdash; see Settings > CI/CD > Deployment Tokens). An example of this for **MS Azure App Services** is provided below.

Once you have an account set up for MS Azure, you can create a new _App Service_ with the following settings:

<img src="./docs/MS_Azure_Deploy_1.png" alt="deploy 1" width="650"/>

To avoid excessive costs, it is recommended to either use the "Basic B1" or "Free F1" deployment plan. This can be created inline by selecting "Change size" at the bottom of the form.

<img src="./docs/MS_Azure_Deploy_2.png" alt="deploy 2" width="650"/>


<img src="./docs/GL1.png" alt="deploy 2" width="650"/>

<img src="./docs/GL2.png" alt="deploy 2" width="650"/>


The docker deployment should be selected from a private repository. Fill in the appropriate details; if you haven't done so already you can obtain a username and password from the "Deployment Token" section in the repository settings (see above). Make sure that you have selected the "read_registry" privilege for your token.

<img src="./docs/MS_Azure_Deploy_3.png" alt="deploy 3" width="650"/>

Review the contents of your deployment and click create. This process takes a while so be patient. Once the resource is provisioned (the app is still probably deploying) you can start by viewing the container settings.

<img src="./docs/MS_Azure_Deploy_Container_Config.png" alt="container config" width="800"/>

Within the container settings you should set the flag for continuous deployment to "On" and copy the _Webhook URL_ to your clipboard. Don't forget to click "save" afterward.

<img src="./docs/MS_Azure_Deploy_General_Config.png" alt="env config" width="800"/>

Once you've done that, you can check the general configuration of the deployment. This is where you can specify environment variables for your application such as database connections strings and third-party authentication credentials.

## Configuration

To configure an external database, you should consider the following:

1. Is the database deployed and accessible by the app service? If you are using Azure SQL Server then there is an option to enable access to the database from other Azure services without enabling access publicly. If possible, you should avoid public access and instead whitelist your own IP address in the "Firewalls and virtual networks" configuration page.
2. The [`appsettings.json`](./appsettings.json) file inside the project **SHOULD _NOT_** have the connection string for the production database configured. ASP.Net Core projects allow settings to be provided with environment variables but they cannot override settings specified in the application settings files.

    Instead, you can configure a development data source in the [`appsettings.Development.json`](./appsettings.Development.json) file that does not use the same connection string name. You then have to ensure this is configured correctly in [`Startup.cs`](./Startup.cs#L31-39).

Once you have configured your application correctly you should provide the appropriate _Connection String_ in the Azure App Service configuration screen (pictured above).

To configure IdentityServer correctly, you must [specify the contents of the `Key` property](https://docs.microsoft.com/en-us/aspnet/core/security/authentication/identity-api-authorization?view=aspnetcore-3.1#deploy-to-production) in the IdentityServer settings inside [`appsettings.json`](./appsettings.json#L15-17). Typically this would involve purchasing a certificate from a certificate authority.

Additionally, while in the general configuration screen in the Azure Portal (pictured above), you should also set an environment variable called `ASPNETCORE_FORWARDEDHEADERS_ENABLED` to the value `true`. [This will allow IdentityServer to operate behind the Azure App Service reverse proxy](https://docs.microsoft.com/en-us/aspnet/core/host-and-deploy/proxy-load-balancer?view=aspnetcore-3.1#forward-the-scheme-for-linux-and-non-iis-reverse-proxies). This can also be configured directly inside [`Startup.cs`](./Startup.cs) but using the environment variable is recommended.

By now the application should have deployed and started running. After configuring the environment variables they must be explicitly saved. This will also trigger the app to restart with the new variables. You can go to the "overview" page to get the application URL and check that its working.

## Database Migrations

The above Azure deployment process does not handle any portion of the database migrations process. This can be done in one of two ways:

- [Configuring migrations to happen automatically at application start.](https://docs.microsoft.com/en-us/ef/core/managing-schemas/migrations/?tabs=dotnet-core-cli#apply-migrations-at-runtime) This is a destructive process that might not be desirable in a low-fidelity deployment.
- Manual migrations using the Entity Framework CLI tool.

For these projects we recommend the manual option.

For manual migrations on the production database, the database connection string must be supplied inside [`appsetttings.Development.json`](./appsetttings.Development.json) before the commands are run. Note that the database connection strings **contain database secrets and should _NOT_** be committed to Git. Once you're done running migrations it is recommended to remove the connection string from the configuration file to prevent accidental uploading to Docker or Git.

## Automatic CI

With the Webhook you copied previously, you can now specify two specific environment variables that are used by the deployment job:

- `DEPLOYMENT_WEBHOOK`
- `DEPLOYMENT_AUTH`

The URL previously copied should look something like this:

```
https://$username:password@accelerate-demo-deployment.scm.azurewebsites.net/docker/hook
```

The value of `DEPLOYMENT_WEBHOOK` can be constructed by removing the `$username:password@` part of the URL; i.e.

```
https://accelerate-demo-deployment.scm.azurewebsites.net/docker/hook
```

The value of `DEPLOYMENT_AUTH` can be constructed by running the following command in a bash terminal:

```bash
echo -n "\$username:password" | base64 --wrap=0
```

Note that the `$` must be escaped to avoid being interpreted as an environment variable.

Once you've generated these values, place them into the CI variables interface on Gitlab.

<img src="./docs/Gitlab_CI_Variables.png" alt="env config" width="800"/>

When this is done, you should be able to trigger redeployments of your application from your Gitlab interface. If you want redeployment to happen automatically, then you can remove this line from `.gitlab-ci.yml`:

```yml
when: manual
```

## Maintainers

[Greg Linklater (@EternalDeiwos)](https://gitlab.com/EternalDeiwos)

## Contributing

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2020 Noroff Accelerate AS
